-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 10:29 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flipkart`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_emailid` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_emailid` varchar(500) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_emailid`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'sujatha@coact.co.in', '2021-01-06 18:28:42', '2021-01-08 09:58:38', '2021-01-08 09:59:08', 1, 'flipkart'),
(2, 'Pawan@coact.co.in', '2021-01-06 18:28:58', '2021-01-06 18:28:58', '2021-01-06 18:29:08', 1, 'flipkart'),
(3, 'nishanth@coact.co.in', '2021-01-06 18:29:28', '2021-01-06 18:52:56', '2021-01-06 18:53:26', 1, 'flipkart'),
(4, 'reynold@coact.co.in', '2021-01-06 18:31:44', '2021-01-06 18:31:44', '2021-01-06 18:46:15', 0, 'flipkart'),
(5, 'naganeerajkurra@gmail.com', '2021-01-06 19:01:32', '2021-01-06 19:01:32', '2021-01-06 19:01:37', 0, 'flipkart'),
(6, 'jay.r@acmeexperience.com', '2021-01-07 01:11:17', '2021-01-08 10:16:50', '2021-01-08 10:17:20', 1, 'flipkart'),
(7, 'amanda.lisa@flipkart.com', '2021-01-07 10:25:37', '2021-01-08 09:50:57', '2021-01-08 09:51:27', 1, 'flipkart'),
(8, 'sumit.sahay@flipkart.com', '2021-01-07 14:24:31', '2021-01-08 10:26:20', '2021-01-08 10:26:50', 1, 'flipkart'),
(9, 'vikas.sharma@flipkart.com', '2021-01-07 14:35:00', '2021-01-07 14:35:00', '2021-01-07 14:35:10', 1, 'flipkart'),
(10, 'ramakrishnan.v@flipkart.com', '2021-01-07 14:56:11', '2021-01-07 14:56:28', '2021-01-07 14:56:58', 1, 'flipkart'),
(11, 'prathap.reddy@flipkart.com', '2021-01-07 15:22:20', '2021-01-08 10:16:09', '2021-01-08 10:16:39', 1, 'flipkart'),
(12, 'akshat@coact.co.in', '2021-01-07 15:48:28', '2021-08-09 14:39:16', '2021-08-09 14:39:46', 1, 'flipkart'),
(13, 'Anshul.s@flipkart.com', '2021-01-08 08:34:31', '2021-01-08 08:34:31', '2021-01-08 08:34:41', 1, 'flipkart'),
(14, 'akshay.kumar@flipkart.com', '2021-01-08 09:08:18', '2021-01-08 11:11:07', '2021-01-08 11:11:37', 1, 'flipkart'),
(15, 'rakesh.shahi@flipkart.com', '2021-01-08 09:54:17', '2021-01-08 09:54:17', '2021-01-08 09:54:48', 0, 'flipkart'),
(16, 'saravanan@lsc-india.com', '2021-01-08 10:00:37', '2021-01-08 10:58:58', '2021-01-08 10:59:28', 1, 'flipkart'),
(17, 'ankit.dewan@flipkart.com', '2021-01-08 10:01:35', '2021-01-08 10:01:35', '2021-01-08 10:01:45', 1, 'flipkart'),
(18, 'prof.ganesan@lsc-india.com', '2021-01-08 10:02:18', '2021-01-08 10:02:18', '2021-01-08 10:03:15', 0, 'flipkart'),
(19, 'vishwas.jain@flipkart.com', '2021-01-08 10:03:14', '2021-01-08 11:02:16', '2021-01-08 11:02:46', 1, 'flipkart'),
(20, 'kishan.sata@flipkart.com', '2021-01-08 10:03:21', '2021-01-08 10:14:01', '2021-01-08 10:14:31', 1, 'flipkart'),
(21, 'gayathri@lsc-india.com', '2021-01-08 10:05:08', '2021-01-08 10:05:08', '2021-01-08 10:05:18', 1, 'flipkart'),
(22, 'guru@lsc-india.com', '2021-01-08 10:06:40', '2021-01-08 10:06:40', '2021-01-08 10:06:50', 1, 'flipkart'),
(23, 'pkrajkumar71@gmail.com', '2021-01-08 10:07:47', '2021-01-08 10:07:47', '2021-01-08 10:07:57', 1, 'flipkart'),
(24, 'dhanab@logskim.com', '2021-01-08 10:09:10', '2021-01-08 11:20:27', '2021-01-08 11:20:57', 1, 'flipkart'),
(25, 'navin@lsc-india.com', '2021-01-08 10:11:57', '2021-01-08 11:03:38', '2021-01-08 11:04:08', 1, 'flipkart'),
(26, 'rohit.rkumar@flipkart.com', '2021-01-08 10:12:08', '2021-01-08 10:12:08', '2021-01-08 10:12:18', 1, 'flipkart'),
(27, 'gokul.raj@Lsc-india.com', '2021-01-08 10:13:01', '2021-01-08 10:13:01', '2021-01-08 10:13:37', 0, 'flipkart'),
(28, 'prashanth@lsc-india.com', '2021-01-08 10:13:01', '2021-01-08 15:17:47', '2021-01-08 15:18:17', 1, 'flipkart'),
(29, 'ram@lsc-india.com', '2021-01-08 10:14:10', '2021-01-08 10:19:55', '2021-01-08 10:20:25', 1, 'flipkart'),
(30, 'madhan@lsc-india.com', '2021-01-08 10:14:37', '2021-01-08 10:14:37', '2021-01-08 10:14:47', 1, 'flipkart'),
(31, 'ram.matlani@flipkart.com', '2021-01-08 10:21:27', '2021-01-08 11:16:45', '2021-01-08 11:17:15', 1, 'flipkart'),
(32, 'punam.dubey@flipkart.com', '2021-01-08 10:22:21', '2021-01-08 10:22:21', '2021-01-08 10:22:31', 1, 'flipkart'),
(33, 'giridharan@lsc-india.com', '2021-01-08 10:23:07', '2021-01-08 11:14:17', '2021-01-08 11:14:47', 1, 'flipkart'),
(34, 'ashish.kp@flipkart.com', '2021-01-08 10:23:26', '2021-01-08 10:40:50', '2021-01-08 10:41:20', 1, 'flipkart'),
(35, 'shivam.mittal@flipkart.com', '2021-01-08 10:23:30', '2021-01-08 10:23:30', '2021-01-08 10:23:40', 1, 'flipkart'),
(36, 'ravi.rai@flipkart.com', '2021-01-08 10:23:40', '2021-01-08 10:27:53', '2021-01-08 10:28:23', 1, 'flipkart'),
(37, 'sheetal.s@flipkart.com', '2021-01-08 10:24:17', '2021-01-08 10:24:17', '2021-01-08 10:24:27', 1, 'flipkart'),
(38, 'lakshmi.m@acmeexperience.com', '2021-01-08 10:24:47', '2021-01-08 10:24:47', '2021-01-08 10:24:57', 1, 'flipkart'),
(39, 'amol.patare@flipkart.com', '2021-01-08 10:24:55', '2021-01-08 11:28:36', '2021-01-08 11:29:06', 1, 'flipkart'),
(40, 'praveer@flipkart.com', '2021-01-08 10:28:31', '2021-01-08 10:28:31', '2021-01-08 11:08:36', 0, 'flipkart'),
(41, 'mahesh.ps@flipkart.com', '2021-01-08 10:30:09', '2021-01-08 10:30:09', '2021-01-08 10:30:19', 1, 'flipkart'),
(42, 'kapilsuhas.n@flipkart.com', '2021-01-08 10:30:17', '2021-01-08 10:30:17', '2021-01-08 10:30:27', 1, 'flipkart'),
(43, 'supriya.bhattacharya@flipkart.com', '2021-01-08 10:30:46', '2021-01-08 10:30:46', '2021-01-08 11:05:27', 0, 'flipkart'),
(44, 'intkhab.alam@flipkart.com', '2021-01-08 10:30:59', '2021-01-08 10:30:59', '2021-01-08 10:31:09', 1, 'flipkart'),
(45, 'raghunandan.g@flipkart.com', '2021-01-08 10:31:01', '2021-01-08 10:31:01', '2021-01-08 10:31:11', 1, 'flipkart'),
(46, 'rishi.diwan@flipkart.com', '2021-01-08 10:31:20', '2021-01-08 10:31:20', '2021-01-08 10:31:30', 1, 'flipkart'),
(47, 'sharique.m@flipkart.com', '2021-01-08 10:32:02', '2021-01-08 10:32:02', '2021-01-08 10:32:12', 1, 'flipkart'),
(48, 'ritesh.bara@flipkart.com', '2021-01-08 10:32:13', '2021-01-08 10:32:13', '2021-01-08 10:32:23', 1, 'flipkart'),
(49, 'naveen.avantikar@flipkart.com', '2021-01-08 10:32:25', '2021-01-08 10:32:25', '2021-01-08 10:32:35', 1, 'flipkart'),
(50, 'jagat.pradip@flipkart.com', '2021-01-08 10:32:41', '2021-01-08 10:32:41', '2021-01-08 10:33:04', 0, 'flipkart'),
(51, 'sandeep.manval@flipkart.com', '2021-01-08 10:34:25', '2021-01-08 10:34:25', '2021-01-08 11:02:58', 0, 'flipkart'),
(52, 'kiran.parmar@flipkart.com', '2021-01-08 10:34:33', '2021-01-08 10:34:33', '2021-01-08 10:34:43', 1, 'flipkart'),
(53, 'mahesh@flipkart.com', '2021-01-08 10:35:32', '2021-01-08 10:54:29', '2021-01-08 10:54:59', 1, 'flipkart'),
(54, 'nikhil.joshi@flipkart.com', '2021-01-08 10:35:53', '2021-01-08 10:35:53', '2021-01-08 10:36:03', 1, 'flipkart'),
(55, 'bapuji.c@flipkart.com', '2021-01-08 10:36:12', '2021-01-08 10:36:12', '2021-01-08 10:36:22', 1, 'flipkart'),
(56, 'abhijan.ghosh@flipkart.com', '2021-01-08 10:36:13', '2021-01-08 10:36:13', '2021-01-08 10:36:23', 1, 'flipkart'),
(57, 'bala@lsc-india.com', '2021-01-08 10:38:18', '2021-01-08 10:55:22', '2021-01-08 10:55:52', 1, 'flipkart'),
(58, 'sujit.k@flipkart.com', '2021-01-08 10:38:51', '2021-01-08 10:38:51', '2021-01-08 11:06:00', 0, 'flipkart'),
(59, 'bharat.kvn@flipkart.com', '2021-01-08 10:39:28', '2021-01-08 10:39:28', '2021-01-08 10:39:38', 1, 'flipkart'),
(60, 'sonal.dongle@flipkart.com', '2021-01-08 10:40:42', '2021-01-08 10:40:42', '2021-01-08 10:40:52', 1, 'flipkart'),
(61, 'nandkishore.k@flipkart.com', '2021-01-08 10:41:02', '2021-01-08 10:41:02', '2021-01-08 10:41:12', 1, 'flipkart'),
(62, 'paromita.paul@flipkart.com', '2021-01-08 10:41:18', '2021-01-08 10:41:18', '2021-01-08 10:53:25', 0, 'flipkart'),
(63, 'wamiq.wadood@flipkart.com', '2021-01-08 10:42:13', '2021-01-08 10:42:13', '2021-01-08 10:42:23', 1, 'flipkart'),
(64, 'srinivasan.r@flipkart.com', '2021-01-08 10:42:18', '2021-01-08 10:42:18', '2021-01-08 10:42:44', 0, 'flipkart'),
(65, 'kamal.das@flipkart.com', '2021-01-08 10:42:43', '2021-01-08 10:42:43', '2021-01-08 10:42:53', 1, 'flipkart'),
(66, 'sonali.rout@flipkart.com', '2021-01-08 10:42:46', '2021-01-08 10:42:46', '2021-01-08 10:42:56', 1, 'flipkart'),
(67, 'dadasaheb.akkole@flipkart.com', '2021-01-08 10:42:49', '2021-01-08 10:43:08', '2021-01-08 10:43:38', 1, 'flipkart'),
(68, 'srividhya.sriram@lsc-india.com', '2021-01-08 10:44:03', '2021-01-08 10:44:03', '2021-01-08 10:44:13', 1, 'flipkart'),
(69, 'chetan.kotyalkar@flipkart.com', '2021-01-08 10:45:14', '2021-01-08 10:45:14', '2021-01-08 10:45:24', 1, 'flipkart'),
(70, 'monika.jp@flipkary.com', '2021-01-08 10:48:29', '2021-01-08 10:48:29', '2021-01-08 10:48:39', 1, 'flipkart'),
(71, 'analkumar.jha@flipkart.com', '2021-01-08 10:48:41', '2021-01-08 10:48:41', '2021-01-08 10:48:51', 1, 'flipkart'),
(72, 'akhil.sharma@flipkart.com', '2021-01-08 10:49:57', '2021-01-08 10:49:57', '2021-01-08 10:50:07', 1, 'flipkart'),
(73, 'm.divyang@flipcart.com', '2021-01-08 10:50:05', '2021-01-08 11:22:24', '2021-01-08 11:22:54', 1, 'flipkart'),
(74, 'saurabh.kala@flipkart.com', '2021-01-08 10:50:23', '2021-01-08 10:50:23', '2021-01-08 10:50:33', 1, 'flipkart'),
(75, 'prabha.kuckian@flipkart.com', '2021-01-08 10:51:26', '2021-01-08 11:21:10', '2021-01-08 11:21:40', 1, 'flipkart'),
(76, 'rajesh@lsc-india.com', '2021-01-08 10:51:34', '2021-01-08 10:51:34', '2021-01-08 10:51:44', 1, 'flipkart'),
(77, 'gayatgri@lsc-india.com', '2021-01-08 10:52:20', '2021-01-08 10:52:20', '2021-01-08 10:52:30', 1, 'flipkart'),
(78, 'mani.n@flipkart.com', '2021-01-08 10:57:48', '2021-01-08 10:57:48', '2021-01-08 10:57:58', 1, 'flipkart'),
(79, 'shahnawaz.khan@flipkart.com', '2021-01-08 10:58:04', '2021-01-08 10:58:04', '2021-01-08 10:58:14', 1, 'flipkart'),
(80, 'fahad.khan@flipkart.com', '2021-01-08 10:58:08', '2021-01-08 10:58:08', '2021-01-08 10:58:18', 1, 'flipkart'),
(81, 'neha.dinesh@flipkart.com', '2021-01-08 10:59:28', '2021-01-08 10:59:28', '2021-01-08 10:59:38', 1, 'flipkart'),
(82, 'sunilkumar.dubey@flipkart.com', '2021-01-08 11:02:05', '2021-01-08 11:02:05', '2021-01-08 11:04:54', 0, 'flipkart'),
(83, 'prabhakar.k@flipkart.com', '2021-01-08 11:03:15', '2021-01-08 11:03:15', '2021-01-08 11:05:29', 0, 'flipkart'),
(84, 'premnath.singh@flipkart.com', '2021-01-08 11:05:54', '2021-01-08 11:05:54', '2021-01-08 11:06:04', 1, 'flipkart'),
(85, 'vikram.shah@flipkart.com', '2021-01-08 11:09:40', '2021-01-08 11:09:40', '2021-01-08 11:09:50', 1, 'flipkart'),
(86, 'bhargav.shah@flipkart.com', '2021-01-08 11:13:06', '2021-01-08 11:13:06', '2021-01-08 11:13:16', 1, 'flipkart'),
(87, 'akash.chopra@flipkart.com', '2021-01-08 11:14:13', '2021-01-08 11:14:13', '2021-01-08 11:14:23', 1, 'flipkart'),
(88, 'deepesh.gattani@flipkart.com', '2021-01-08 11:14:32', '2021-01-08 11:15:03', '2021-01-08 11:15:33', 1, 'flipkart'),
(89, 'richa.bharti@flipkart.com', '2021-01-08 11:38:54', '2021-01-08 11:38:54', '2021-01-08 11:39:09', 0, 'flipkart'),
(90, 'abhishek.nandy@flipkart.com', '2021-01-08 11:44:40', '2021-01-08 11:44:40', '2021-01-08 11:44:50', 1, 'flipkart'),
(91, 'ramanujam@lsc-india.com', '2021-01-08 12:25:26', '2021-01-08 12:25:26', '2021-01-08 12:25:36', 1, 'flipkart');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
