<?php

	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $emailid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$emailid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

          
            unset($_SESSION["user_emailid"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flipkart Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body class="bg">

<div class="container-fluid">
    <div class='row '>
		<div class="col-md-7 offset-md-3">
		<img src="img/FCE_Video_Page-top.jpg" class="img-fluid">
		</div>
		
	</div>
   
	<div class=" row d-sm-block d-md-none">
	<br/>
	<br/>
	<br/>
	<br/>
	
	
	</div>
    <div class="row video-panel">

        <div class="col-12 col-md-8 offset-md-2 ">
            <div class="embed-responsive embed-responsive-16by9">
			
              <iframe class="embed-responsive-item" id="webcast" src="video.php" allowfullscreen></iframe>
            
			</div> 
				
        </div>
		<div class="col-md-2 text-right">
		<a href="?action=logout" class="btn btn-sm btn-danger ">Logout</a>
		</div>
		
        
    </div>
	

</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>