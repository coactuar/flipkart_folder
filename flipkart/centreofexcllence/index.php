<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flipkart</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>

input[type=email]{
	border-radius : 10px ;
	border: 2px solid #aba5a5;
}
</style>
</head>

<body>
<div class="container-fluid">
    <div class="row content">
	<div class="col-md-4 offset-md-4">
	<img src="img/Floginpagetop.jpg" class="img-fluid" >
	</div>
        <div class="col-10 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-1">
            <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  
                  
                  <div class="input-group">
                    <input type="email" class="form-control " placeholder="Email ID" aria-label="Email ID" aria-describedby="basic-addon1" name="emailid" id="emailid" required>
                  </div>
                  <div class="input-group mt-md-2">
                    <button  class="login-button" type="submit"><img src="img/button.png" class="img-fluid" ></button>
                  </div>
                  
                 
            </form>        
        </div>
    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>


$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     
      if(data = 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
          return false;
      }
	 
  });
  
  return false;
});
</script>
</body>
</html>